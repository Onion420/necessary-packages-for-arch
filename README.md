# Necessary packages for arch

This project aims to create a BASH script used to finish up the installation procedures for arch. You should have a working base install already.

TODO:

- User Creation: Basic implementation is done
- Package auto install: Partially finished, more will be added in the future
- Install a graphical environment 
- Download, build, and install a selected AUR helper (E.G. Pikaur, Yay, etc.)
- Install AUR packages 
- Tidy up the script
