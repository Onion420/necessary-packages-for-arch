#!/bin/bash

## Authored by Anthony T
## Gitlab: https://gitlab.com/Onion420
## Run this as root so it can do it's thing


## Asks the user for the desired username and password

echo -n "Enter Desired Username
"
read username

echo -n "Enter Desired Password
"
read password

## Changes root password

passwd $password

## Creates the user using the useradd command (no arguements for now)

useradd $username -p $password

## Creates wheel group and adds previously created user

groupadd wheel

gpasswd -a $username wheel

## Installs the sudo package

pacman -Syy

pacman -S sudo

## At this point you must edit the sudoers file yourself

## Updates repositories and Installs packages

pacman -S sudo cinnamon firefox steam lutris termite wine-staging vulkan-icd-loader vulkan-radeon lib32-vulkan-icd-loader lib32-vulkan-radeon


